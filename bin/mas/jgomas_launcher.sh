#!/usr/bin/env sh

par1="lib/jade.jar:lib/jadeTools.jar:lib/Base64.jar:lib/http.jar:lib/iiop.jar:lib/beangenerator.jar:lib/jgomas.jar:lib/jason.jar:lib/JasonJGomas.jar:classes:."

par2="jade.Boot -container -local-host 127.0.0.1"

ag_ax1="T1:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax2="T2:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax3="T3:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS_FIELDOPS.asl)"
ag_ax4="T4:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS_CRAZY.asl)"
ag_ax5="T5:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS_FOLLOWER.asl)"
ag_al1="A1:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_MEDIC.asl)"
ag_al2="A2:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED.asl)"
ag_al3="A3:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_MEDIC.asl)"
ag_al4="A4:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_CRAZY_KILLER.asl)"
ag_al5="A5:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_SHOOTER.asl)"
ag_al6="A6:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_SCARED.asl)"

if [ -z "$1" ]; then
    agentes="$ag_ax1;$ag_ax2;$ag_ax3;$ag_ax4;$ag_ax5;$ag_al1;$ag_al2;$ag_al3;$ag_al4;$ag_al5;$ag_al6"
else
    agentes="$ag_ax1;$ag_ax2;$ag_ax3;$ag_ax4;$ag_ax5;$ag_al1;$ag_al2;$ag_al3;$ag_al4"
fi


java -classpath "$par1" $par2 "$agentes"
