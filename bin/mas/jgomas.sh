#!/bin/sh

if [ -z "$1" ]; then
    num_ag=""
    task4=""
else
    num_ag=9
    task4="task4"
fi

cleanup () {
    echo "KILLING MANAGER"
    pkill -P $manager_id
    killall java
}
trap cleanup INT

#############
## Manager ##
#############

sh ./jgomas_manager.sh $num_ag &
manager_id="$$"

###############################################################################

sleep 4

##############
## Launcher ##
##############

sh ./jgomas_launcher.sh $task4

###############################################################################
